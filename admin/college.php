<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Recent</title>
</head>
<script type="application/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script>
    var imageResult = null;

    function showColleges(colleges) {
        var oTable = $('#college_table > tbody')[0];
        row = oTable.insertRow($('#college_table tbody > tr').length);

        col = row.insertCell(0);
        col.innerHTML = "Image";
        col = row.insertCell(1);
        col.innerHTML = "Name";
        col = row.insertCell(2);
        col.innerHTML = "Likes";
        col = row.insertCell(3);
        col.innerHTML = "Action";

        $.each(colleges, function(k, v) {
            var row, col;

            row = oTable.insertRow($('#college_table tbody > tr').length);

            col = row.insertCell(0);
            var imgUrl = '/almafind/uploads/college/' + v.photoId;
            col.innerHTML = "<img width=300px height=200px src='" + imgUrl + "'/>";
            col = row.insertCell(1);
            col.innerHTML = "<a href='college_detail.php?collegeId=" + v.collegeId + "'>" + v.collegeName + "</a>";
            col = row.insertCell(2);
            col.innerHTML = "likes: " + v.likes + "<br/> followers: " + v.followers;
            col = row.insertCell(3);
            col.innerHTML = "<input type='button' onclick='removeCollege(" + v.collegeId + ");' value='remove'/>";
        });
    }

    function removeCollege(college_id) {
        var url = "/almafind/api/v1/colleges/" + college_id;
        $.ajax({
            type: "DELETE",
            url: url,
            complete: function (response) {
                location.href = "college.php";
            }
        });
    }

    function getColleges() {
        $.ajax({
            type: "GET",
            url: "/almafind/api/v1/colleges",
            dataType: "application/json; charset=utf-8",
            complete: function (response) {
                var college_list = jQuery.parseJSON(response.responseText);
                showColleges(college_list);
            }
        });
    }

    function handleImagePreview() {
        var college_image_file = $('#college_image_file')[0];

        college_image_file.onchange = function(e) {
            var file = college_image_file.files[0];
            var img = $('#college_image')[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (event) {
                imageResult = event.target.result;
                img.src = imageResult;
            };
        };
    }

    function onCollegeAddClicked() {
        if (imageResult == null) {
            alert('Please select image file');
            return;
        }
        var $status = $('#status')[0];
        var file = $('#college_image_file')[0].files[0];
        var fileName = file.name;
        var collegeName = $('#college_name').val();
        var postData = { data: imageResult, fileName: fileName, collegeName: collegeName };
        $.ajax({
            type: 'post',
            url: '/almafind/api/v1/colleges',
            data: postData,
            dataType: 'application/json',
            complete: function (response) {
                var college = $.parseJSON(response.responseText);
                location.href = 'college.php';
            }
        });
        $status.innerHTML = 'uploading image...';
    }

    $(document).ready(function() {
        getColleges();
        handleImagePreview();
    });
</script>
<body>
<div align="center">
    <div><h1>almafind<h1></div>
    <div><a href="index.php">Home</a></div>
    <div>
        <form>
            <table>
                <tr>
                    <td><label for="college_name">College name: </label></td>
                    <td><input type="text" id="college_name" placeholder="Enter a name of college."/></td>
                </tr>
                <tr>
                    <td><label for="college_image_file">College Image: </label></td>
                    <td><input type="file" id="college_image_file"/></td>
                </tr>
                <tr>
                    <td colspan="2"><div id="status"></div></td>
                </tr>
                <tr>
                    <td colspan="2"><img width='300px' height='auto' src="" id="college_image"></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="button" value="Add" onclick="onCollegeAddClicked();"></td>
                </tr>
            </table>
        </form>
    </div>
    <table id="college_table" style="text-align: center">
        <tbody></tbody>
    </table>
</div>
</body>
</html>


