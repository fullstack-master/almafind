<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Recent</title>
</head>
<script type="application/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script>
    function showStudents(students) {
        var oTable = $('#student_table > tbody')[0];
        row = oTable.insertRow($('#student_table tbody > tr').length);

        col = row.insertCell(0);
        col.innerHTML = "Image";
        col = row.insertCell(1);
        col.innerHTML = "Name";
        col = row.insertCell(2);
        col.innerHTML = "e-mail";
        col = row.insertCell(3);
        col.innerHTML = "Action";

        $.each(students, function(k, v) {
            var row, col;

            row = oTable.insertRow($('#student_table tbody > tr').length);

            col = row.insertCell(0);
            var imgUrl = '/almafind/uploads/photos/' + v.photoId;
            col.innerHTML = "<img width=300px height=200px src='" + imgUrl + "'/>";
            col = row.insertCell(1);
            col.innerHTML = v.userName;
            col = row.insertCell(2);
            col.innerHTML = v.email;
            col = row.insertCell(3);
            col.innerHTML = "<input type='button' onclick='blockStudent(" + v.userId + ");' value='remove'/>";
        });
    }

    function blockStudent(student_id) {
        var url = "/almafind/api/v1/admin/students/" + student_id;
        $.ajax({
            type: "DELETE",
            url: url,
            complete: function (response) {
                location.href = "college.php";
            }
        });
    }

    function getStudents() {
        $.ajax({
            type: "GET",
            url: "/almafind/api/v1/admin/students",
            dataType: "application/json; charset=utf-8",
            complete: function (response) {
                var student_list = jQuery.parseJSON(response.responseText);
                showStudents(student_list);
            }
        });
    }

    $(document).ready(function() {
        getStudents();
    });
</script>
<body>
<div align="center">
    <div><h1>almafind<h1></div>
    <div><a href="index.php">Home</a></div>
    <table id="student_table" style="text-align: center">
        <tbody></tbody>
    </table>
</div>
</body>
</html>


