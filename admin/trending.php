<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Recent</title>
</head>
<script type="application/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script>
    var imageResult = null;

    function showTrendingItem(trending_list) {
        var oTable = $('#trending_item_table > tbody')[0];
        row = oTable.insertRow($('#trending_item_table tbody > tr').length);

        col = row.insertCell(0);
        col.innerHTML = "Image";
        col = row.insertCell(1);
        col.innerHTML = "Tag";
        col = row.insertCell(2);
        col.innerHTML = "Trending Title";
        col = row.insertCell(3);
        col.innerHTML = "Action";

        $.each(trending_list, function(k, v) {
            var row, col;

            row = oTable.insertRow($('#trending_item_table tbody > tr').length);

            col = row.insertCell(0);
            var imgUrl = '/almafind/uploads/trending/' + v.imageName;
            col.innerHTML = "<img width=300px height=200px src='" + imgUrl + "'/>";
            col = row.insertCell(1);
            col.innerHTML = v.tag;
            col = row.insertCell(2);
            col.innerHTML = "<a href='videos.php?trendingId=" + v.tag + "'>" + v.trendingName + "</a>";
            col = row.insertCell(3);
            col.innerHTML = "<input type='button' onclick='removeTrendingItem(" + v.id + ");' value='remove'/>";
        });
    }

    function removeTrendingItem(id) {
        $.ajax({
            type: "DELETE",
            url: "/almafind/api/v1/admin/trending/" + id,
            complete: function (response) {
                location.href = "trending.php";
            }
        });
    }

    function getTrendingItem() {
        $.ajax({
            type: "GET",
            url: "/almafind/api/v1/admin/trending",
            dataType: "application/json; charset=utf-8",
            complete: function (response) {
                var trending_list = jQuery.parseJSON(response.responseText);
                showTrendingItem(trending_list);
            }
        });
    }

    function handleImagePreview() {
        $('#trending_image_file')[0].onchange = function(e) {
            var file = $('#trending_image_file')[0].files[0];
            var img = $('#trending_image')[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (event) {
                imageResult = event.target.result;
                img.src = imageResult;
            };
        };
    }

    function onTrendingAddClicked() {
        if (imageResult == null) {
            alert('Please select image file');
            return;
        }
        var $status = $('#status')[0];
        var file = $('#trending_image_file')[0].files[0];
        var fileName = file.name;
        var trendingName = $('#trending_name').val();
        $.ajax({
            type: 'post',
            url: '/almafind/api/v1/admin/trending',
            data: { data: imageResult, fileName: fileName, trendingName: trendingName },
            dataType: 'application/json',
            complete: function (response) {
                var trending = $.parseJSON(response.responseText);
                location.href = 'trending.php';
            }
        });
        $status.innerHTML = 'uploading image...';
    }

    $(document).ready(function() {
        getTrendingItem();
        handleImagePreview();
    });
</script>
<body>
<div align="center">
    <div><h1>almafind<h1></div>
    <div><a href="index.php">Home</a></div>
    <div>
        <form>
            <table>
                <tr>
                    <td><label for="trending_name">Trending name: </label></td>
                    <td><input type="text" id="trending_name" placeholder="Enter a name for new trending item."/></td>
                </tr>
                <tr>
                    <td><label for="trending_image_file">Trending Image: </label></td>
                    <td><input type="file" id="trending_image_file"/></td>
                </tr>
                <tr>
                    <td colspan="2"><div id="status"></div></td>
                </tr>
                <tr>
                    <td colspan="2"><img width='300px' height='auto' src="" id="trending_image"></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="button" value="Add" onclick="onTrendingAddClicked();"></td>
                </tr>
            </table>
        </form>
    </div>
    <table id="trending_item_table" style="text-align: center">
        <tbody></tbody>
    </table>
</div>
</body>
</html>


