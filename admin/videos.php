<?php
$trendingId = isset($_GET['trendingId']) ? $_GET['trendingId']: '';
?>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Recent</title>
</head>
<script type="application/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script>
    var trendingId = '<?php echo $trendingId; ?>';
    function showVideos(video_list) {
        var oTable = $('#videos_table > tbody')[0];
        var row = oTable.insertRow($('#videos_table tbody > tr').length);

        var col = row.insertCell(0);
        col.innerHTML = "Video";
        col = row.insertCell(1);
        col.innerHTML = "Caption";
        col = row.insertCell(2);
        col.innerHTML = "Poster";
        col = row.insertCell(3);
        col.innerHTML = "Trending";
        col = row.insertCell(4);
        col.innerHTML = "Action";

        $.each(video_list, function(k, v) {
            var row, col;

            row = oTable.insertRow($('#videos_table tbody > tr').length);

            col = row.insertCell(0);
            col.innerHTML = '<video id="' + v.id +'" width="320" height="240" controls></video>';
            var videoPlayer = $('#' + v.id)[0];
            var source = document.createElement('source');
            source.setAttribute('src', '/almafind/uploads/videos/' + v.videoUrl);
            videoPlayer.appendChild(source);

            col = row.insertCell(1);
            col.innerHTML = "<a href='videos_detail.php?videoId=" + v.videoId + "'>" + v.caption + "</a>";
            col = row.insertCell(2);
            col.innerHTML = v.userName;
            col = row.insertCell(3);
            col.innerHTML = v.trendingName;
            col = row.insertCell(4);
            col.innerHTML = "<input type='button' onclick=removeVideos('" + v.id + "') value='remove'/>";
        });
    }

    function removeVideos(id) {
        $.ajax({
            type: "DELETE",
            url: "/almafind/api/v1/admin/videos/" + id,
            complete: function (response) {
                location.href = "videos.php";
            }
        });
    }

    function getVideos(trendingId) {
        var url = '';
        if (trendingId == '')
            url = "/almafind/api/v1/videos";
        else
            url = "/almafind/api/v1/videos/trending/" + trendingId;

        $.ajax({
            type: "GET",
            url: url,
            dataType: "application/json; charset=utf-8",
            complete: function (response) {
                var videos = jQuery.parseJSON(response.responseText);
                showVideos(videos.data);
            }
        });
    }

    $(document).ready(function() {
        getVideos(trendingId);
    });
</script>
<body>
<div align="center">
    <div><h1>almafind<h1></div>
    <div><a href="index.php">Home</a></div>
    <div>
    </div>
    <table id="videos_table" style="text-align: center">
        <tbody>
        </tbody>
    </table>
</div>
</body>
</html>


