<?php
$videoId = isset($_GET['videoId']) ? $_GET['videoId']: '';
?>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Recent</title>
</head>
<script type="application/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script>
    var videoId = '<?php echo $videoId; ?>';

    function showVideo(video) {
        $('#videoUrl').val(video.videoUrl);
        $('#caption').val(video.caption);
        $('#poster').val(video.userName);
        $('#selectTrending').val(video.trendingId);
        $('#videoSource')[0].src = '/almafind/uploads/videos/' + video.videoUrl;
        $('#videoPlayer')[0].load();
    }

    function getTrendingItem() {
        $.ajax({
            type: "GET",
            url: "/almafind/api/v1/admin/trending",
            dataType: "application/json; charset=utf-8",
            complete: function (response) {
                var trending_list = jQuery.parseJSON(response.responseText);
                $.each(trending_list, function (k, v) {
                    $('#selectTrending').append('<option value="' + v.tag + '">' + v.trendingName + '</option>');
                });
                getVideo(videoId);
            }
        });
    }

    function getVideo(videoId) {
        $.ajax({
            type: "GET",
            url: "/almafind/api/v1/videos/id/" + videoId,
            dataType: "application/json; charset=utf-8",
            complete: function (response) {
                var video = jQuery.parseJSON(response.responseText);
                showVideo(video);
            }
        });
    }

    function onSelectTrendingClicked() {
        var trendingId = $('#selectTrending').val();
        var trendingName = $('#selectTrending option:selected').text();
        if (trendingId == null) trendingName = '';

        var data = JSON.stringify({ "trendingId": trendingId, "trendingName": trendingName });

        $.ajax({
            type: "POST",
            url: "/almafind/api/v1/admin/videos/" + videoId + "/trending",
            contentType: "application/json; charset=utf-8",
            dataType: "application/json; charset=utf-8",
            data: data,
            complete: function (response) {
                var video = jQuery.parseJSON(response.responseText);
                showVideo(video);
            }
        });
    }

    $(document).ready(function() {
        getTrendingItem();
    });
</script>
<body>
<div align="center">
    <div><h1>almafind<h1></div>
    <div><a href="index.php">Home</a></div>
    <div>
    </div>
    <table>
        <tr>
            <td colspan="3">
                <label for="video">Video:</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <video id="videoPlayer" width="320" height="240" controls>
                    <source id="videoSource" src="">
                </video>
            </td>
        </tr>
        <tr>
            <td>
                <label for="caption">Caption:</label>
            </td>
            <td>
                <input id="caption" type="text" value="" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label for="caption">Poster:</label>
            </td>
            <td>
                <input id="poster" type="text" value="" readonly>
            </td>
        </tr>
        <tr>
            <td>
                <label for="trending">Trending:</label>
            </td>
            <td>
                <select id="selectTrending">
                    <option value="">Not selected</option>'
                </select>
                <input type="button" value="Select" onclick="onSelectTrendingClicked();">
            </td>
        </tr>
        <tr>
            <td>
                <label for="caption">Add Hashtags:</label>
            </td>
            <td>
                <input id="hashtag" type="text" name="hashtag" value="" placeholder="hashtag">
                <input id="addHashTag" type="button" name="addHashtag" value="add">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ul id="hashtags">
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <label for="tagSchool">Tag School</label>
            </td>
            <td>
                <select id="tagSchool" name="tagSchool"></select>
            </td>
        </tr>
    </table>
</div>
</body>
</html>


