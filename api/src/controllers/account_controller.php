<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require_once 'basic_controller.php';
require_once 'http_status_codes.php';
require_once 'api_response.php';
require_once __DIR__ . '/../models/user.php';
require_once __DIR__ . '/../models/auth_info.php';
require_once __DIR__ . '/../models/payment.php';
require_once __DIR__ . '/../models/verify.php';

class AccountController extends BasicController {
    private $model;

    function __construct(\Interop\Container\ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->model = new User($this->db);
    }


    public function login(Request $request, Response $response, $args) {
        $apiResponse = new ApiResponse($response);

        $credential = $request->getHeader('Authorization');
        if ($credential == null) {
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'Please login first.');
        }

        $email = strtolower($_SERVER['PHP_AUTH_USER']);
        $password = $_SERVER['PHP_AUTH_PW'];

        $this->logger->debug('email: '.$email);

        $user = $this->model->signIn($email, $password);
        if (!$user) {
            return $apiResponse->error(HttpStatusCodes::HTTP_UNAUTHORIZED, '');
        }

        // Now login success, we generate auth token and return this to client
        $authModel = new AuthInfo($this->db);

        $authInfo = $authModel->getByUserId($user['userId']);
        if (!$authInfo) {
            $authInfo =  $authModel->entity();
            $authInfo['userId'] = $user['userId'];
            $authInfo = $authModel->insert($authInfo);
        }

        $authInfo['token'] = $authModel->generate();
        $authInfo['modified'] = date('Y-m-d h:m:s');;

        if (!$authModel->update($authInfo)) {
            return $apiResponse->error(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, 'update failed');
        }

        $_SESSION['user_id'] = $authInfo['userId'];

        return $apiResponse->data('authInfo', $authInfo);
    }

    public function logout(Request $request, Response $response, $args) {
        $apiResponse = new ApiResponse($response);

        $auth_token = $request->getHeader('Authorization');
        if ($auth_token == null)
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'Missing token');

        $result = AuthInfo::get_by_token($auth_token[0]);
        if (!$result['result'])
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'Invalid token');

        $auth = $result['auth'];
        $result = AuthInfo::reset($auth['token']);
        if (!$result['result'])
            return $apiResponse->error(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, $result['error']);

        return $apiResponse->data('authInfo', array());
    }

    public function signUp(Request $request, Response $response, $args) {
        $apiResponse = new ApiResponse($response);

        $parsedBody = $request->getParsedBody();
        $description = $parsedBody['description'];
        $this->logger->debug('description: '.$description);

        $user = $this->model->entity(json_decode($description, true));
        $user['email'] = strtolower($user['email']);

        // check whether a user exists with email address.
        $existing = $this->model->find('email', $user['email']);
        if ($existing) {
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'User already exists.');
        }

        // move user's photo image and assign to user
        $files = $request->getUploadedFiles();
        $file = $files['image'];
        if ($file->getError() === UPLOAD_ERR_OK) {
            $ext = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
            $user['photoId'] = md5(uniqid(rand(), true)).".".$ext;
            $this->logger->debug('photoId: '.$user['photoId']);

            $targetPath = $this->photoPath.$user['photoId'];
            $file->moveTo($targetPath);
        } else {
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'Upload error');
        }

        // set additional properties for the user.
        $user['created'] = date('Y-m-d h:m:s');
        $user['userId'] = md5(uniqid(rand(), true));
        $user['tags'] = '';

        // create customer object of stripe
        $customer = Payment::createSimpleCustomer($user['email']);
        if (!$customer) {
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'Creating customer failed');
        }
        $user['customerId'] = $customer['id'];
        $user['password'] = new NotORM_Literal("PASSWORD('".$user['password']."')");

        $user = $this->model->insert($user);
        if (!$user) {
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'Database error');
        }

        return $apiResponse->data('user_id', $user['userId']);
    }

    public function me(Request $request, Response $response, $args) {
        $apiResponse = new ApiResponse($response);
        $auth_token = $request->getHeader('Authorization');

        if ($auth_token == null)
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'Missing token');

        $authModel = new AuthInfo($this->db);
        $auth = $authModel->find('token', $auth_token[0]);
        if (!$auth)
            return $apiResponse->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'Invalid token');

        $user = $this->model->find('userId', $auth['userId']);
        if (!$user)
            return $apiResponse->error(HttpStatusCodes::HTTP_NOT_FOUND, '');

        $user['password'] = '';
        return $apiResponse->data('user', $user);
    }

    public function getUserById(Request $request, Response $response, $args) {
        $user = $this->model->find('userId',$args['id']);
        if (!$user) {
            return $response->withStatus(HttpStatusCodes::HTTP_BAD_REQUEST, '');
        }

        return $this->apiResponse($response, $user);
    }

    public function uploadPhotoInJSON(Request $request, Response $response, $args) {
        $data = file_get_contents($_POST['data']);

        $uploadFileName = uniqid().'.png';
        $fp = fopen(__DIR__ . '/../../../uploads/photos/' .$uploadFileName, 'w');
        fwrite($fp, $data);
        fclose($fp);

        return $this->apiResponse($response, ['photoId' => $uploadFileName]);
    }

    public function uploadPhoto(Request $request, Response $response, $args) {
        $files = $request->getUploadedFiles();
        if (empty($files['userPhoto'])) {
            $response->withStatus(HttpStatusCodes::HTTP_BAD_REQUEST, 'missing file');
        }

        $photoFile = $files['userPhoto'];
        if ($photoFile->getError() === UPLOAD_ERR_OK) {
            $uploadFileName = uniqid();
            $photoFile->moveTo("../../../uploads/photos/$uploadFileName");
            return $this->apiResponse($request, ['photoId' => $uploadFileName]);
        } else {
            return $response->withStatus(HttpStatusCodes::HTTP_BAD_REQUEST, 'File upload error');
        }
    }

    public function verify(Request $request, Response $response, $args) {
        $user = $this->currentUser($request);

        $verifyCode = strval(rand(1000, 9999));

        $result = $this->model->sendVerifyMail($user, $verifyCode);
        if ($result['code'] != 'success') {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, $result['message']);
        }
/*
        $result = [
            'code' => 'success',
            'message' => 'message',
            'data' => ['message-id' => 'message-id']
        ];
        */
        $this->logger->debug('verifyCode: '.$verifyCode);

        // todo: First check verify code exists with not expired time. And if there exists response with error.
        $verifyModel = new VerifyModel($this->db);
        $verify = $verifyModel->entity();
        $verify['userId'] = $user['userId'];
        $verify['verifyCode'] = $verifyCode;
        $verify['verifyToken'] = md5(uniqid(rand(), true));
        $verify['code'] = $result['code'];
        $verify['message'] = $result['message'];
        $verify['messageId'] = $result['data']['message-id'];
        $verify['generatedAt'] = date('Y-m-d H:i:s');
        $verify['expireVerifyCode'] = date('Y-m-d H:i:s', strtotime('+10 minutes'));

        $verify = $verifyModel->insert($verify);
        if (!$verify) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, 'Database error');
        }

        $this->logger->debug('verifyCode: '.json_encode($verify));

        return $this->apiResponse($response, $verify);
    }

    public function confirm(Request $request, Response $response, $args) {
        $api_response = new ApiResponse($response);

        $user = $this->currentUser($request);
        if (!$user) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, 'User');
        }
        $user['verified'] = 1;
        $user = $this->model->update($user);
        if (!$user) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, 'Database error');
        };

        return $api_response->data('user_id', $user['userId']);
    }
}