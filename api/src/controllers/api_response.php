<?php

require_once 'http_status_codes.php';

/**
 * @param $object_name
 * @return array
 */
class ApiResponse
{
    private $response_data;
    private $response;

    function __construct($response)
    {
        $api_response = array();

        $api_response['id'] = '';
        $api_response['object'] = '';
        $api_response['data'] = 0;
        $api_response['count'] = 0;
        $api_response['status'] = 0;
        $api_response['error'] = '';

        $this->response_data = $api_response;
        $this->response = $response;
    }

    public function error($status, $error) {
        $this->response_data['status'] = $status;
        $this->response_data['error'] = $error;

        return $this->jsonResponse();
    }

    public function data($object, $data) {
        $this->response_data['object'] = $object;
        $this->response_data['data'] = $data;
        if ($object == 'list')
            $this->response_data['count'] = count($data);

        $this->response_data['status'] = HttpStatusCodes::HTTP_OK;
        return $this->jsonResponse();
    }

    private function jsonResponse() {
        $this->response->getBody()->write(json_encode($this->response_data));
        return $this->response->withHeader('Content-type', 'application/json');
    }
}

