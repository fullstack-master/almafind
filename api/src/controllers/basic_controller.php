<?php
use Psr\Http\Message\ResponseInterface as Response;

require_once 'http_status_codes.php';
require_once __DIR__ . '/../models/basic_model.php';
require_once __DIR__ . '/../models/auth_info.php';
require_once __DIR__ . '/../models/user.php';

class BasicController {
    protected $ci;
    protected $logger;
    protected $user;
    protected $settings;
    protected $db;
    protected $uploadPath;
    protected $uploadVideoPath;
    protected $thumbnailPath;
    protected $photoPath;

    //Constructor
    public function __construct(Interop\Container\ContainerInterface $ci) {
        $this->logger = $ci->get('logger');
        $this->settings = $ci->get('settings');
        $this->db = $ci->get('db');

        $this->uploadPath = $this->settings['uploadPath'];
        $this->uploadVideoPath = $this->uploadPath."videos/";
        $this->thumbnailPath = $this->uploadPath."thumbnails/";
        $this->photoPath = $this->uploadPath."photos/";

        $this->ci = $ci;
    }

    function checkAuth($request) {
        $authToken = $request->getHeader('Authorization');
        if (isset($authToken)) {
            return (new AuthInfo($this->db))->find('token', $authToken[0]);
        }
        return false;
    }

    function currentUser($request) {
        $auth = $this->checkAuth($request);
        if ($auth) {
            $userModel = new User($this->db);
            return $userModel->find('userId', $auth['userId']);
        }

        return false;
    }

    public function apiResponse(Response $response, $data = '', $status = HttpStatusCodes::HTTP_OK) {
        if (is_array($data)) {
            $this->logger->debug('data: '.json_encode($data));
            return $response->withJson($data, $status);
        } else {
            $response->getBody()->write($data);
            return $response->withStatus($status, strval($data));
        }
    }
}