<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require_once 'basic_controller.php';
require_once 'http_status_codes.php';
require_once 'api_response.php';

require_once __DIR__ . '/../models/college.php';
require_once __DIR__ . '/../models/video.php';

class CollegeController extends BasicController {
    private $model;

    function __construct(\Interop\Container\ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->model = new College($this->db);
        $this->logger->debug('Upload Path: ' + $this->uploadPath);
    }

    public function get(Request $request, Response $response, $args) {
        $params = $request->getQueryParams();
        if (isset($params['name'])) {
            $result = $this->model->find('collegeName', $params['name']);
            if (!$result) {
                return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, $params['name']);
            }
        } else {
            $result = $this->model->getAll();
        }

        return $this->apiResponse($response, $result);
    }

    public function getVideos(Request $request, Response $response, $args) {
        $collegeId = $args['collegeId'];
        $category = $args['category'];

        $college = $this->model->find('collegeId', $collegeId);
        if (!$college) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, $collegeId);
        }

        $collegeName = $college['collegeName'];
        $videoModel = new Video($this->db);
        $result = $videoModel->getCollegeVideos($collegeName, $category);
        if (!$result) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, $collegeName.':'.$category);
        }

        return $this->apiResponse($response, $result);
    }

    public function getTags(Request $request, Response $response, $args) {
        $this->apiResponse($response, $this->model->getAll());
    }

    public function delete(Request $request, Response $response, $args) {
        $college_id = $args['college_id'];

        // before remove record, delete file.
        $college = $this->model->find('collegeId', $college_id);
        if (!$college) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, $college_id);
        }

        $photoUrl = $college['photoId'];
        if ($photoUrl != '') {
            $photoPath = $this->uploadPath.'/college/'.$photoUrl;
            unlink($photoPath);
        }

        // TODO: reset trending for associated videos

        // remove record
        $result = $this->model->delete($college['id']);
        if (!$result) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, '');
        }

        return $this->apiResponse($response, $college);
    }

    public function insert(Request $request, Response $response, $args) {
        // TODO: check the name for the college
        $data = file_get_contents($_POST['data']);

        $uploadFileName = $_POST['fileName'];
//        $uploadFileName = uniqid().'.png';
        $photoPath = $this->uploadPath.'/college/'.$uploadFileName;
        $fp = fopen($photoPath, 'w'); //Prepends timestamp to prevent overwriting
        fwrite($fp, $data);
        fclose($fp);

        $college = $this->model->entity();
        $college['collegeName'] = $_POST['collegeName'];
        $college['photoId'] = $uploadFileName;
        $college['collegeId'] = strval(time());
//            $college['tag'] = $_POST['collegeName'];
        $college['tag'] = '';
        $college['likes'] = 0;
        $college['followers'] = 0;

        $college = $this->model->insert($college);
        if (!$college) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, '');
        }

        return $this->apiResponse($response, $college);
    }
}