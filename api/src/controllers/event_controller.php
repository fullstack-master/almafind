<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require_once 'basic_controller.php';
require_once 'http_status_codes.php';
require_once 'api_response.php';
require_once __DIR__ . '/../models/event.php';
require_once __DIR__ . '/../models/payment.php';

class EventController extends BasicController {
    private $model;
    private $userModel;

    function __construct(\Interop\Container\ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->model = new Event($this->db);
        $this->userModel = new User($this->db);
    }

    public function handleEvent(Request $request, Response $response, $args) {

        // Retrieve the request's body and parse it as JSON
        $input = @file_get_contents("php://input");
        $event = json_decode($input, true);

        $this->logger->debug('event: '.$input);

        $verifiedEvent = Payment::verifyEvent($event);
        if ($verifiedEvent == null) {
            http_response_code(HttpStatusCodes::HTTP_BAD_REQUEST);
        }

        $this->logger->debug('verified event: '.$verifiedEvent);

        $event = $this->model->insert($this->model->transEvent($event));

        $ret = 0;

        $this->logger->debug($verifiedEvent->type);

        switch($verifiedEvent->type) {
            case "account.updated":
                break;
            case "customer.created":
                break;
            case "invoice.payment_succeeded":
                // update active_until field
                $invoice = $verifiedEvent->data->object;
                $user = $this->userModel->find('customerId', $invoice->customer);
                if ($user) {
                    $last_renewal = date("Y-m-d H:i:s");
                    $next_renewal = date('Y-m-d H:i:s', strtotime("+30 days", strtotime($last_renewal)));
                    $user['active_until'] = $next_renewal;
                    if (!$this->userModel->update($user)) {
                        $ret = 1;
                    };
                } else {
                    $this->logger->debug('could not found the user');
                    $ret = 1;
                }
                break;
        }

        if (!$ret)
            http_response_code(HttpStatusCodes::HTTP_OK);
        else
            http_response_code(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR);
    }
}