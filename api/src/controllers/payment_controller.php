<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require_once 'http_status_codes.php';
require_once 'api_response.php';
require_once __DIR__ . '/../models/user.php';
require_once __DIR__ . '/../models/auth_info.php';
require_once __DIR__ . '/../models/event.php';
require_once __DIR__ . '/../models/payment.php';

class PaymentController extends BasicController {
    private $model;
    private $userModel;

    function __construct(\Interop\Container\ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->model = new Payment();
        $this->userModel = new User($this->db);
    }

    public function retrieveCustomer(Request $request, Response $response, $args) {
        $customer = Payment::retrieveCustomer($this->currentUser($request)['customerId']);
        if ($customer != null) {
            return $response->withStatus(200, '');
        } else {
            return $response->withStatus(402, '');
        }
    }

    public function attachSourceToCustomer(Request $request, Response $response, $args) {
        $customerId = $this->currentUser($request)['customerId'];
        $card = Payment::attachSourceToCustomer($customerId, $_POST["source"]);
        if ($card != null) {
            return $response->withStatus(200, '');
        } else {
            return $response->withStatus(402, '');
        }
    }

    public function selectDefaultCustomerSource(Request $request, Response $response, $args) {
        $customerId = $this->currentUser($request)['customerId'];
        $customer = Payment::selectDefaultCustomerSource($customerId, $_GET['default_source']);
        if ($customer != null) {
            return $response->withStatus(200, '');
        } else {
            return $response->withStatus(402, '');
        }
    }

    public function getMethods(Request $request, Response $response, $args) {
        $customerId = $this->currentUser($request)['customerId'];
        if (!isset($customerId) || $customerId == '') {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, 'The customer not registered');
        }

        $methods = Payment::getPaymentMethods($customerId);
        if (!$methods) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, '');
        }

        return $this->apiResponse($response, $methods);
    }

    public function addMethod(Request $request, Response $response, $args) {
        $user = $this->currentUser($request);
        if (!$user) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, 'The user not found');
        }

        $tokenId = $request->getParsedBody()['tokenId'];

        // create customer with card or add new payment method.
        $customerId = $user['customerId'];
        if (!isset($customerId) || $customerId == '') {
            // if the user have not customerId yet then create new customer on stripe and link it to the customer
            $customer = Payment::createSimpleCustomer($user['email']);
            if (!$customer) {
                return $response->withStatus(HttpStatusCodes::HTTP_BAD_GATEWAY, 'Cannot create customer');
            }

            // get created customer
            $customerId = $user['customerId'] = $customer->id;

            // update the user with created customerId
            $user = $this->userModel->update($user);
            if (!$user) {
                return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, '');
            }
        }

        $this->logger->debug('$customerId: ' . $customerId);

        // if the user have a stripe customer Id, try add a new payment method
        $card = $this->model->addPaymentMethod($customerId, [ "source" => $tokenId ]);
        if (!$card) {
            return $response->withStatus(HttpStatusCodes::HTTP_BAD_GATEWAY, '');
        }

        return $this->apiResponse($response, ['status' => 'ok']);
    }

    public function addCharge(Request $request, Response $response, $args) {
        $jsonObj = $request->getParsedBody();

        if (!isset($jsonObj['amount']) ||
            !isset($jsonObj['source'])) {
            return $response->withStatus(HttpStatusCodes::HTTP_BAD_REQUEST, 'Invalid parameter.');
        }

        $amount = $jsonObj['amount'] * 100;
        $cardToken = $jsonObj['source'];

        // get customer Id
        $user = $this->currentUser($request);
        if (!$user) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, '');
        }
        $customerId = $user['customerId'];
        if (!isset($customerId)) {
            return $response->withStatus(HttpStatusCodes::HTTP_BAD_REQUEST, 'There are not exists a customer information for certain user.');
        }

        // find Card Id
        $cardId = '';
        $description = 'Charge for '.$user['email'];
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            // get customer and card objects
            $customer = \Stripe\Customer::retrieve($customerId);

            foreach ($customer['sources']['data'] as $source) {
                $hashedToken = md5($source['id'], false);
                if ($cardToken == $hashedToken) {
                    $cardId = $source['id'];
                    break;
                }
            }
            if ($cardId == '') {
                return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, 'The card not found: ');
            }

            $charge = \Stripe\Charge::create([
                'customer' => $customerId,
                'amount' => $amount, // Amount in cents!
                'currency' => 'usd',
                'source' => $cardId,
                'description' => $description
            ]);
        } catch (\Stripe\Error\Card $e) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        return $this->apiResponse($response, [
            'charge_id' => $charge['id'],
            'amount' => $charge['amount'] / 100,
            'exp_month' => $charge['source']['exp_month'],
            'exp_year' => $charge['source']['exp_year'],
            'status' => $charge['status']
        ]);
    }

    public function getCoupon(Request $request, Response $response, $args) {
        $coupon_id = $args['coupon_id'];

        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            // get customer and card objects
            $coupon = \Stripe\Coupon::retrieve($coupon_id);
        } catch (\Stripe\Error\Card $e) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        return $this->apiResponse($response, $coupon);
    }

    public function getCustomer(Request $request, Response $response, $args) {
        $api_response = new ApiResponse($response);

        // get user object with userId
        $user = $this->userModel->find('userId', $args['userId']);
        if (!$user) {
            return $api_response->error(HttpStatusCodes::HTTP_NOT_FOUND, '');
        }
        $customerId = $user['customerId'];
        if (!isset($customerId)) {
            return $api_response->error(HttpStatusCodes::HTTP_BAD_REQUEST, 'No customer information.');
        }

        $methods = array();
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            $customer = \Stripe\Customer::retrieve($customerId);
            $default_source = $customer['default_source'];

            $paymentMethod = array();
            foreach ($customer['sources']['data'] as $source) {
                $paymentMethod['cardToken'] = md5($source['id'], false);
                $paymentMethod['brand'] = $source['brand'];
                $paymentMethod['last4'] = $source['last4'];
                if ($default_source == $source['id'])
                    $paymentMethod['default'] = 'true';
                else
                    $paymentMethod['default'] = 'false';

                $methods[] = $paymentMethod;
            }
        } catch (\Stripe\Error\Card $e) {
            return $api_response->error(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        return $api_response->data('list', $methods);
    }

    public function getPlans(Request $request, Response $response, $args) {
        $plans = Payment::retrievePlans();
        if (!$plans) {
            return $response->withStatus(HttpStatusCodes::HTTP_BAD_GATEWAY, 'Invalid parameter.');
        }

        return $this->apiResponse($response, $plans);
    }

    public function subscribeCustomer(Request $request, Response $response, $args) {
        $user = $this->currentUser($request);
        $params = $request->getParsedBody();
        $plan = $params['plan'];
        $card = $params['card'];
        $coupon = $params['coupon'];

        if ($user['customerId'] == '') {
            return $response->withStatus(HttpStatusCodes::HTTP_BAD_REQUEST, 'The customer missing.');
        }

        $subscription = Payment::subscribeCustomer($user['customerId'], $plan, $card, $coupon);
        if (!$subscription) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, '');
        }

        // store
        return $this->apiResponse($response, $subscription);
    }
}