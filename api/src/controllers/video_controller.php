<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require_once 'basic_controller.php';
require_once 'http_status_codes.php';
require_once 'api_response.php';
require_once __DIR__ . '/../models/college.php';
require_once __DIR__ . '/../models/trending.php';
require_once __DIR__ . '/../models/video.php';

function execInBackground($cmd) {
    if (substr(php_uname(), 0, 7) == "Windows") {
        $handle = popen($cmd, "r");
        $result = "";
        while (!feof($handle)) {
            $read = fread($handle, 64 * 1024);
            $result .= $read;
        }

        pclose($handle);
        return $result;
    }
    else {
        $result = exec($cmd . " > /dev/null &");
        return $result;
    }
}

class VideoController extends BasicController {
    private $model;

    function __construct(\Interop\Container\ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->model = new Video($this->db);
    }

    /**
     * Get all of videos
     */
    public function getAll($request, $response, $args) {
        $api_response = new ApiResponse($response);
        return $api_response->data('list', $this->model->getAll());
    }

    public function uploadForMobile(Request $request, Response $response, $args) {
        $parsedBody = $request->getParsedBody();
        $description = $parsedBody['description'];

        $video = $this->model->entity(json_decode($description, true));

        $files = $request->getUploadedFiles();
        $videoFile = $files['video'];
        $imageFile = $files['image'];

        if ($videoFile->getError() === UPLOAD_ERR_OK) {

            $ext = pathinfo($videoFile->getClientFilename(), PATHINFO_EXTENSION);

            $video['fileName'] = $videoFile->getClientFilename();
            $video['videoUrl'] = md5(uniqid(rand(), true)).".".$ext;
            $video['videoId'] = md5(uniqid(rand(), true));
            $video['uploadedAt'] = date('Y-m-d h:m:s');

            $targetPath = $this->uploadVideoPath.$video['videoUrl'];
            $videoFile->moveTo($targetPath);
        } else {
            return $this->apiResponse($response, [
                'result' => 'error',
                'message' => 'The uploading of the video was failed. Please check the size of the video file.',
                'videoId' => ''
            ]);
        }

        if ($imageFile->getError() === UPLOAD_ERR_OK) {

            $targetPath = $this->thumbnailPath.$video['videoId'].'.jpg';
            $imageFile->moveTo($targetPath);
            // save thumbnail
            $video['thumbnail'] = $video['videoId'].'.jpg';
        } else {
            return $this->apiResponse($response, [
                'result' => 'error',
                'message' => 'The uploading of the thumbnail was failed. Please check the size of the image file.',
                'videoId' => ''
            ]);
        }

        $video = $this->model->insert($video);
        if (!$video) {
            return $this->apiResponse($response, [
                'result' => 'error',
                'message' => 'Database error.',
                'videoId' => ''
            ]);
        }

        return $this->apiResponse($response, [
            'result' => 'ok',
            'message' => 'Video was uploaded successfully',
            'videoId' => $video['videoId']
        ]);
    }

    public function generateThumbnail(Request $request, Response $response, $args) {
        $videoId = $args['video_id'];

        $video = $this->model->find('videoId', $videoId);
        if (!$video) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, '');
        }

        $videoFile = $this->uploadVideoPath.$video['videoUrl'];

        $second = 1;
        $image  = $this->thumbnailPath.$video['videoId'].'.jpg';
        $cmd = __DIR__ . "/ffmpeg -i $videoFile -deinterlace -an -ss $second -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $image";
        $output = execInBackground($cmd);
        $this->logger->debug('get video info: '.$cmd);
        $this->logger->debug('result of generating thumbnail image: '.$output);

        // save thumbnail
        $video['thumbnail'] = $videoId.'.jpg';

        $video = $this->model->update($video);
        if (!$video) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, 'update() failed');
        }

        return $this->apiResponse($response, $video);
    }

    public function uploadVideoInfo(Request $request, Response $response, $args) {
        $data = file_get_contents($_POST['data']);
        $fileName = $_POST['name'];
        $postToken = $_POST['postToken'];

        // todo: check postToken
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $uploadFileName = uniqid().".".$ext;
        $fp = fopen($this->uploadVideoPath.$uploadFileName, 'w'); //Prepends timestamp to prevent overwriting
        fwrite($fp, $data);
        fclose($fp);

        return $this->apiResponse($response, [
            'videoUrl' => $uploadFileName
        ]);
    }

    public function uploadVideo($request, $response, $args) {
        $json_obj = $request->getParsedBody();

        $video = $this->model->entity($json_obj);

        $video['uploadedAt'] = date('Y-m-d h:m:s');
        $video['videoId'] = md5(uniqid(rand(), true));

        $video = $this->model->insert($video);
        if (!$video) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, '');
        }

        return $this->apiResponse($response, [
            'result' => 'ok',
            'message' => 'Video was uploaded successfully',
            'videoId' => $video['videoId']
        ]);
    }

    public function uploadTest(Request $request, Response $response, $args) {
        $json_obj = $request->getParsedBody();

        $video = $this->model->entity($json_obj);

        $video['fileName'] = 'test.mp4';
        $video['videoUrl'] = md5(uniqid(rand(), true)).".mp4";
        $video['videoId'] = md5(uniqid(rand(), true));
        $video['uploadedAt'] = date('Y-m-d h:m:s');
        $video['thumbnail'] = md5(uniqid(rand(), true));

        $video = $this->model->insert($video);
        if (!$video) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, '');
        }

        return $this->apiResponse($response, [
            'result' => 'ok',
            'message' => 'Video was uploaded successfully',
            'videoId' => $video['videoId']
        ]);
    }

    public function getVideoById($request, $response, $args) {
        $api_response = new ApiResponse($response);

        $video = $this->model->get($args['id']);
        if (!$video) {
            return $api_response->error(HttpStatusCodes::HTTP_NOT_FOUND, '');
        }

        return $api_response->data('video', $video);
    }

    public function getVideoByVideoId(Request $request, Response $response, $args) {
        $video = $this->model->getVideo($args['video_id']);
        if (!$video) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, '');
        }

        return $this->apiResponse($response, $video);
    }

    public function getRecentVideos(Request $request, Response $response, $args) {
        $query_params = $request->getQueryParams();
        $count = isset($query_params['count']) ? $query_params['count']: 3;

        $params = $request->getParsedBody();
        $scope = $params['scope'];

        $video = $this->model->getRecent($count, $scope);
        if (!$video) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, '');
        }

        return $this->apiResponse($response, $video);
    }

    public function getTrendingItems(Request $request, Response $response, $args) {
        return $this->apiResponse($response, (new Trending($this->db))->getAll());
    }

    public function getAllTrendingVideos(Request $request, Response $response, $args) {
        return $this->apiResponse($response, $this->model->getByTrending($args['trendingId']));
    }

    public function getTrendingVideos(Request $request, Response $response, $args) {
        return $this->apiResponse($response, $this->model->getByTrending($args['trendingId']));
    }

    public function setScope(Request $request, Response $response, $args) {
        $auth = $this->checkAuth($request);
        $scope = json_encode($request->getParsedBody()['scope']);
        if (!$this->model->setDiscoverScope($auth['token'], $scope)) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, 'Set Discover Scope failed');
        }

        return $this->apiResponse($response, $scope);
    }

    public function like(Request $request, Response $response, $args) {
        $this->checkAuth($request);
        $like = $request->getParsedBody();
        $userId = $like['userId'];
        $videoId = $like['videoId'];

        $video = $this->model->like($userId, $videoId);
        if (!$video) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, 'Like failed');
        }

        return $this->apiResponse($response, $video);
    }

    public function deleteById(Request $request, Response $response, $args) {
        $id = $args['id'];
        $video = $this->model->find('id', $id);
        if (!$video) {
            return $response->withStatus(HttpStatusCodes::HTTP_NOT_FOUND, 'Not Found');
        }

        $result = $this->model->delete($video['id']);
        if (!$result) {
            return $response->withStatus(HttpStatusCodes::HTTP_INTERNAL_SERVER_ERROR, 'Database error');
        }

        // remove the video file
        $targetPath = $this->uploadVideoPath.$video['videoUrl'];
        if (file_exists($targetPath)) {
            if (!unlink($targetPath)) {
                $this->logger->debug('could not delete the file '.$targetPath);
            }
        }

        // remove the video file
        $targetPath = $this->thumbnailPath.$video['thumbnail'];
        if (file_exists($targetPath)) {
            if (!unlink($targetPath)) {
                $this->logger->debug('could not delete the file '.$targetPath);
            }
        }

        return $this->apiResponse($response, $video);
    }
}