<?php

namespace Slim\Extras\Middleware;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

require_once __DIR__ . '/../models/auth_info.php';

class AuthMiddleware
{
    //Constructor
    public function __construct(array $options = [])
    {
    }

    function check_auth($request) {
        $auth_token = $request->getHeader('Authorization');
        if (!isset($auth_token)) {
            $this->logger->debug('Authorization header node defined.');
            return null;
        }

        $this->logger->debug('check_auth: '.$auth_token);

        $result = AuthInfo::get_by_token($auth_token[0]);
        if (!$result['result']) {
            return null;
        }

        return $result['data'];
    }

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        $auth = $this->check_auth($request);
        if ($auth == null) {
            return $response->withStatus(HttpStatusCodes::HTTP_UNAUTHORIZED, 'Authorization failed.');
        }
        return $next($request, $response);
    }
}
