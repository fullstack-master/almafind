<?php

require_once 'mysql.php';
require_once 'basic_model.php';

class AuthInfo extends BasicModel {

    function __construct($db)
    {
        parent::__construct($db);
        $this->tableName = 'auth_info';
        $this->fields = [
          'id',
          'userId',
          'token',
          'modified'
        ];
    }

    public static function generate() {
        return md5(uniqid(rand(), true));
    }

    /**
     * @param $token
     * @return array
     */
    public static function reset($token) {
        $ret = array();

        $ret['result'] = false;
        $ret['error'] = '';

        $db = connect_db();
        $sql = "UPDATE authinfo SET token = '' WHERE token = ?;";

        try {
            $stmt = $db->prepare($sql);
            $stmt->bind_param('s', $token);
            if ($stmt->execute())
                $ret['result'] = true;
            else
                $ret['result'] = false;
        } catch (mysqli_sql_exception $e) {
            $ret['error'] = $db->error;
        }

        return $ret;
    }

    public function getByUserId($userId) {
        return $this->find('userId', $userId);
    }
}