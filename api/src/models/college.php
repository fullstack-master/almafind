<?php
require_once 'mysql.php';
require_once 'basic_model.php';

class College extends BasicModel {
    public $id;
    public $collegeName = '';
    public $collegeId = '';
    public $photoId = '';
    public $description = '';
    public $likes = 0;
    public $followers = 0;
    public $tag = '';

    function __construct($db)
    {
        parent::__construct($db);
        $this->tableName = 'colleges';
        $this->fields = [
            'id',
            'collegeName',
            'overview',
            'photoId',
            'collegeId',
            'likes',
            'followers',
            'tag'
        ];
    }

    public function getAll() {
        $result = [];
        foreach ($this->db->{$this->tableName}()->order("collegeName ASC") as $record) {
            $result[] = $this->entity($record);
        }
        return $result;
    }

    public function deleteCollege($collegeId) {
        $college = $this->find('collegeId', $collegeId);
        if (!$college) {
            return false;
        }

        return $this->delete($college['id']);
    }
}
