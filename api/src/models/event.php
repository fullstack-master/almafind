<?php
require_once 'mysql.php';
require_once 'basic_model.php';

class Event extends BasicModel {

    function __construct($db)
    {
        parent::__construct($db);
        $this->tableName = 'stripe_events';
        $this->fields = [
            'id',
            'object',
            'event_id',
            'api_version',
            'created',
            'event_data',
            'livemode',
            'pending_webhooks',
            'request',
            'event_type',
        ];
    }

    public function transEvent($stripe_event) {
        $event = $this->entity();

        $event['object'] = $stripe_event['object'];
        $event['event_id'] = $stripe_event['id'];
        $event['api_version'] = $stripe_event['api_version'];
        $event['created'] = $stripe_event['created'];
        $event['event_data'] = json_encode($stripe_event['data']);
        $event['livemode'] = $stripe_event['livemode'];
        $event['pending_webhooks'] = $stripe_event['pending_webhooks'];
        $event['request'] = $stripe_event['request'];
        $event['event_type'] = $stripe_event['type'];

        return $event;
    }
}