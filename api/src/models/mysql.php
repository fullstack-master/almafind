<?php
// Ensure reporting is setup correctly
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

$db_host = getenv('DB_HOST');
$db_user = getenv('DB_USER');
$db_pass = getenv('DB_PASS');
$db_name = getenv("DB_NAME");

function connect_db() {
    global $db_host;
    global $db_user;
    global $db_pass;
    global $db_name;

    $server = $db_host;
    $user = $db_user;
    $pass = $db_pass;
    $database = $db_name;
    $connection = new mysqli($server, $user, $pass, $database);

    return $connection;
}
