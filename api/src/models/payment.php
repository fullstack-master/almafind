<?php
require_once 'user.php';
require_once 'auth_info.php';

$stripe_pk = getenv("STRIPE_PRIVATE_KEY");

// Stripe Private Key
define('STRIPE_PRIVATE_KEY', $stripe_pk);

class Payment {

    /**
     * @param $email
     * @return array
     */
    public static function createSimpleCustomer($email) {
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            $customer = \Stripe\Customer::create([
                'description' => 'Customer for '.$email,
                'email' => $email
            ]);

            return $customer;
        } catch (\Stripe\Error\Card $e) {
            return false;
        }
    }

    /**
     * @param $email
     * @param $source
     * @return array
     */
    public static function createCustomer($email, $source) {
        $ret = array();

        $ret['result'] = false;
        $ret['error'] = '';
        $params = array(
            'description' => 'Customer for '.$email,
            'source' => $source,
        );

        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            $customer = \Stripe\Customer::create($params);
            $ret['customer'] = $customer;
            $ret['result'] = true;
        } catch (\Stripe\Error\Card $e) {
            $ret['error'] = $e->getMessage();
        }

        return $ret;
    }

    public static function retrieveCustomer($customer_id) {
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);
            $customer = \Stripe\Customer::retrieve($customer_id);
            return $customer;
        } catch(\Stripe\Error $e) {
            return null;
        }
    }

    public static function attachSourceToCustomer($customer_id, $source) {
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(array("source" => $source));
            return $card;
        } catch(\Stripe\Error $e) {
            return null;
        }
    }

    public static function selectDefaultCustomerSource($customer_id, $default_source) {
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);
            $customer = \Stripe\Customer::retrieve($customer_id);
            $customer->default_source = $_GET["default_source"];
            $customer->save();
            return $customer;
        } catch(\Stripe\Error $e) {
            return null;
        }
    }

    /**
     * @param $customerId
     * @param $source
     * @return mixed
     */
    public static function addPaymentMethod($customerId, $source) {
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            $customer = \Stripe\Customer::retrieve($customerId);
            return $customer->sources->create($source);
        } catch (\Stripe\Error\Card $e) {
            return false;
        }
    }

    public static function getPaymentMethods($customerId) {
        $methods = array();
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            $customer = \Stripe\Customer::retrieve($customerId);
            $default_source = $customer['default_source'];

            $paymentMethod = array();
            foreach ($customer['sources']['data'] as $source) {
                $paymentMethod['id'] = $source['id'];
                $paymentMethod['brand'] = $source['brand'];
                $paymentMethod['last4'] = $source['last4'];
                if ($default_source == $source['id'])
                    $paymentMethod['default'] = 'true';
                else
                    $paymentMethod['default'] = 'false';

                $methods[] = $paymentMethod;
            }

        } catch (\Stripe\Error\Card $e) {
            // log
        }

        return $methods;
    }

    public static function createPlan($amount, $interval, $name, $currency, $id) {
        $ret = array();

        $ret['result'] = false;
        $ret['error'] = '';

        try{
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            \Stripe\Plan::create(array(
                    "amount" => $amount,
                    "interval" => $interval,
                    "name" => $name,
                    "currency" => $currency,
                    "id" => $id)
            );
            $ret['result'] = true;
        } catch (\Stripe\Error\Card $e) {
            $ret['error'] = $e->getMessage();
        }

        return $ret;
    }

    public static function retrievePlans() {
        try{
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);
            $plans = \Stripe\Plan::all(array("limit" => 1));
            return $plans['data'];
        } catch (\Stripe\Error\Card $e) {
            return false;
        }
    }

    public static function subscribeCustomer($customerId, $plan, $card, $coupon) {
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            $cu = \Stripe\Customer::retrieve($customerId);
            $cu->default_source = $card;
            $cu->save();

            $options = array(
                "customer" => $customerId,
                "plan" => $plan,
            );

            if ($coupon != '') {
                $options['coupon'] = $coupon;
            }
            $subscription = \Stripe\Subscription::create();
            return $subscription;
        } catch (\Stripe\Error\Card $e) {
            return false;
        }
    }

    public static function verifyEvent($event) {
        try {
            \Stripe\Stripe::setApiKey(STRIPE_PRIVATE_KEY);

            // Verify the event by fetching it from Stripe
            $verified_event = \Stripe\Event::retrieve($event['id']);
        } catch (Exception $e) {
            return null;
        }

        return $verified_event;
    }

    public static function onInvoicePaymentSucceeded($invoice) {

        $user = User::findByCustomerId($invoice->customer);
        if ($user == null) {
            return 500;
        }

        if (isset($user['active_until'])) {
            $last_renewal = $user['active_until'];
        } else {
            $last_renewal = date("Y-m-d H:i:s");
        }

        $next_renewal = date('Y-m-d H:i:s', strtotime("+30 days", strtotime($last_renewal)));
        $user['active_until'] = $last_renewal;

        return 0;
    }
}
