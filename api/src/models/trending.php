<?php
require_once 'mysql.php';
require_once 'basic_model.php';

class Trending extends BasicModel {

    function __construct($db)
    {
        parent::__construct($db);
        $this->tableName = 'trending';
        $this->fields = [
            'id',
            'trendingName',
            'tag',
            'imageName',
            'fileName'
        ];
    }

    public function add($trendingName, $imageName, $fileName) {
        $trending = $this->entity();

        $trending['trendingName'] = $trendingName;
        $trending['tag'] = strval(time());
        $trending['imageName'] = $imageName;
        $trending['fileName'] = $fileName;

        return $this->insert($trending);
    }
}