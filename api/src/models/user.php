<?php
//use \PDO;

require_once 'mysql.php';
require_once 'basic_model.php';
require_once __DIR__ . '/../libs/mailin.php';

define('SENDINBLUE_API_KEY', '0MjLR72qd13AKTFm');
define('FROM_EMAIL', 'denisha@pluckedadmissions.org');
define('REPLY_EMAIL', 'yuri.bond@post.com');

class User extends BasicModel {

    function __construct($db)
    {
        parent::__construct($db);
        $this->tableName = 'users';
        $this->fields = [
            'id',
            'userName',
            'photoId',
            'email',
            'password',
            'overview',
            'verified',
            'online',
            'userId',
            'tags',
            'created',
            'lastLogin',
            'likes',
            'follows',
            'customerId',
            'subscriptionId',
            'active_until',
        ];
    }

    public function sendVerifyMail($user, $code) {
        /*
         * This will initiate the API with the endpoint and your access key.
         *
         */
        $mailin = new Mailin('https://api.sendinblue.com/v2.0', SENDINBLUE_API_KEY);

        /*
         * This will send a transactional email
         *
         */
        /** Prepare variables for easy use **/

        $data = array(
            "to" => array($user['email'] => "To ".$user['userName']),
            "from" => array(FROM_EMAIL, "AlmaFind Support Team"),
            "replyto" => array(),
            "subject" => "User verification",
            "text" => "Your verification code is ".$code.".",
            "html" => "<h1>Verification Code</h1><br/>".$code,
            "attachment" => array(),
            "headers" => array(
                "Content-Type" => "text/html; charset=iso-8859-1",
                "X-Mailin-Tag" => "Almafind"
            ),
            "inline_image" => array()
        );

        $result = $mailin->send_email($data);

        return $result;
    }

    /**
     * @param $email
     * @param $password
     * @return array
     */
    public function signIn($email, $password) {
        $result = $this->db->{$this->tableName}->where("email = ? AND password = PASSWORD(?)", $email, $password);

        if ($user = $result->fetch()) {
            return $this->entity($user);
        } else {
            return false;
        }
    }
}
