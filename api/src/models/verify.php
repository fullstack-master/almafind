<?php

require_once 'mysql.php';
require_once 'basic_model.php';


class VerifyModel extends BasicModel {

    function __construct($db)
    {
        parent::__construct($db);
        $this->tableName = 'verify';
        $this->fields = [
            'id',
            'userId',
            'verifyCode',
            'verifyToken',
            'postToken',
            'generatedAt',
            'expireVerifyCode',
            'expirePostCode',
            'code',
            'message',
            'messageId',
        ];
    }

    public function getVerifyCode($userId, $verifyCode, $verifyToken) {
        $result = $this->db->{$this->tableName}->find([
            'userId' => $userId,
            'verifyCode' => $verifyCode,
            'verifyToken'=> $verifyToken
        ]);

        if ($entity = $result->fetch()) {
            return $this->entity($entity);
        } else {
            return false;
        }
    }

    public function generatePostCode($userId, $verifyToken) {
        $postToken = md5(uniqid(rand(), true));
        $generatedAt = date('Y-m-d H:i:s');
        $expirePostCode = date('Y-m-d H:i:s', strtotime('+1 hours', $generatedAt));

        $sql = "UPDATE verify SET postToken = ?, generatedAt = ?, expirePostCode = ?".
            " WHERE userId = ? AND verifyToken = ?;";
        try {
            $stmt = $this->conn->prepare( $sql );
            $stmt->bind_param(
                'sssss',
                $postCode, $generatedAt, $expirePostCode, $userId, $verifyToken
            );

            $stmt->execute();
            return $postToken;
        } catch (mysqli_sql_exception $e) {
            return false;
        }
    }
}