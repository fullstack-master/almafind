<?php
require_once 'mysql.php';
require_once 'basic_model.php';

function time_from_now($dt) {
    $datetime2 = new DateTime();
    $datetime1 = new DateTime($dt);
    $interval = $datetime2->diff($datetime1);

    if ($interval->y > 0) {
        $str = $interval->format('%y years ago');
    } else if ($interval->m > 0) {
        $str = $interval->format('%m months ago');
    } else if ($interval->d > 0) {
        $str = $interval->format('%d days ago');
    } else if ($interval->h > 0) {
        $str = $interval->format('%h hours ago');
    } else if ($interval->m > 0) {
        $str = $interval->format('%i minutes ago');
    } else if ($interval->y > 0) {
        $str = $interval->format('%s seconds ago');
    } else {
        $str = 'just now';
    }

    return $str;
}

class Video extends BasicModel
{
    function __construct($db)
    {
        parent::__construct($db);
        $this->tableName = 'videos';
        $this->fields = [
            'id',
            'caption',
            'videoId',
            'videoUrl',
            'fileName',
            'thumbnail',
            'posterId',
            'userName',
            'hashTags',
            'college',
            'collegeId',
            'uploadedAt',
            'lastViewed',
            'likes',
            'trendingId',
            'trendingName'
        ];
    }

    public function getAll() {
        $sql = "SELECT videos.*, users.userName FROM users, videos WHERE users.userId = videos.posterId ORDER BY uploadedAt DESC;";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->get_result();
            $rows = array();
            while ($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
        } catch (mysqli_sql_exception $e) {
            return false;
        }

        return $rows;
    }

    public function get($id) {
        $sql = "SELECT videos.*, users.userName FROM users, videos WHERE users.userId = videos.posterId AND id = ?;";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result->num_rows > 0)
                return $result->fetch_assoc();
        } catch (mysqli_sql_exception $e) {
            return false;
        }

        return false;
    }

    public function getVideo($videoId) {
        $sql = "SELECT videos.*, users.userName FROM users, videos WHERE users.userId = videos.posterId AND videoId = ?;";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bind_param('s', $videoId);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result->num_rows > 0)
                return $result->fetch_assoc();
        } catch (mysqli_sql_exception $e) {
            return false;
        }

        return false;
    }

    /**
     * @param $video_id
     * @return mixed
     */
    public function deleteVideo($video_id) {
        $sql = "DELETE FROM videos WHERE videoId = ?;";

        try {
            $stmt = $this->conn->prepare( $sql );
            $stmt->bind_param('s', $video_id);
            return $stmt->execute();
        } catch (mysqli_sql_exception $e) {
            return false;
        }

        return $ret;
    }

    /**
     * @param $count
     * @param $scope
     * @return array
     */
    public function getRecent($count, $scope) {
        $rows = array();

        $sql = "SELECT * FROM videos ORDER BY uploadedAt DESC LIMIT 50;";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
        } catch (mysqli_sql_exception $e) {
            return $rows;
        }

        $filtered = array();
        foreach ($rows as $row) {
            foreach ($scope as $collegeId) {
                if ($row['collegeId'] == $collegeId) {
                    $filtered[] = $row;
                }
            }
        }

        return $filtered;
    }

    /**
     * @param $trendingId
     * @return mixed
     */
    public function getByTrending($trendingId) {
        $rows = array();
        $sql = "SELECT videos.*, users.userName FROM users, videos WHERE users.userId = videos.posterId AND trendingId = ?;";

        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("s", $trendingId);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * @param $collegeName
     * @param $category
     * @return array|null
     */
    public function getCollegeVideos($collegeName, $category) {
        $rows = array();
        try {
            if ($category == 'popular') {
                $sql = "SELECT videos.*, users.userName FROM users, videos ".
                    "WHERE users.userId = videos.posterId AND videos.college = ? ORDER BY uploadedAt DESC;";
            } else if ($category == 'recent') {
                $sql = "SELECT videos.*, users.userName FROM users, videos ".
                    "WHERE users.userId = videos.posterId AND videos.college = ? ORDER BY likse DESC;";
            } else {
                return [];
            }

            $stmt = $this->conn->prepare($sql);
            $stmt->bind_param("s", $collegeName);
            $stmt->execute();
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                $comment = time_from_now($row['uploadedAt'])." - ".$row['userName'];
                $row['comment'] = $comment;
                $rows[] = $row;
            }
        } catch (mysqli_sql_exception $e) {
            // log
        }

        return $rows;
    }

    /**
     * @param $token
     * @param $scope
     * @return array|null
     */
    public function setDiscoverScope($token, $scope) {
        $sql = "REPLACE INTO discover (token, scope) VALUES (?, ?)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("ss", $token, $scope);
        $stmt->execute();
        return true;
    }

    public function like($userId, $videoId) {
        $sql = "UPDATE videos SET likes = likes + 1 WHERE videoId = ?";

        // increase likes of the video
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bind_param("s", $videoId);
            if (!$stmt->execute())
                return false;
        } catch (mysqli_sql_exception $e) {
            return false;
        }

        // increase likes of the user
        $sql = "UPDATE users SET likes = likes + 1 WHERE userId = ?";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bind_param("s", $userId);
            if (!$stmt->execute())
                return false;
        } catch (mysqli_sql_exception $e) {
            return false;
        }

        $sql = "SELECT * FROM videos WHERE videoId = ?;";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bind_param("s", $videoId);
            if (!$stmt->execute())
                return false;
            $result = $stmt->get_result();
            if ($result->num_rows > 0) {
                return $result->fetch_assoc();
            }

        } catch (mysqli_sql_exception $e) {
            return false;
        }

        return false;
    }
}
