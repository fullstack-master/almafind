<?php
require_once __DIR__ . '/controllers/account_controller.php';
require_once __DIR__ . '/controllers/college_controller.php';
require_once __DIR__ . '/controllers/video_controller.php';
require_once __DIR__ . '/controllers/payment_controller.php';
require_once __DIR__ . '/controllers/event_controller.php';

// API group
$app->group('/almafind/api/v1', function () use ($app) {

    $app->get('/login', '\AccountController:login');
    $app->post('/signup', '\AccountController:signUp');

    $app->group('/users', function () use ($app) {
        $app->get('/logout', '\AccountController:logout');
        $app->get('/me', '\AccountController:me');
//        $app->get('/{id}', '\AccountController:getUserById');
        $app->post('/upload_photo_json', '\AccountController:uploadPhotoInJSON');
        $app->post('/upload_photo', '\AccountController:uploadPhoto');
        $app->get('/verify', '\AccountController:verify');
        $app->get('/confirm', '\AccountController:confirm');
    });

    $app->group('/colleges', function () use ($app) {
        $app->get('', '\CollegeController:get');
        $app->get('/{collegeId}/{category}', '\CollegeController:getVideos');
        $app->get('/tags', '\CollegeController:getTags');
        $app->delete('/{college_id}', '\CollegeController:delete');
        $app->post('', '\CollegeController:insert');
    });

    $app->group('/videos', function () use ($app) {

        $app->get('', 'VideoController:getAll');
        $app->post('/mobile', '\VideoController:uploadForMobile');
        $app->post('/test', '\VideoController:uploadTest');
        $app->get('/thumbnail/{video_id}', '\VideoController::generateThumbnail');
        $app->post('/upload', '\VideoController:uploadVideoInfo');
        $app->post('/', '\VideoController:uploadVideo');
        $app->get('/{id:[0-9]+}', '\VideoController:getVideoById');
        $app->get('/id/{video_id}', '\VideoController:getVideoByVideoId');
        $app->post('/recent', '\VideoController:getRecentVideos');
        $app->get('/trendings', '\VideoController:getTrendingItems');
        $app->get('/trendings/{trendingId}', '\VideoController:getTrendingVideos');
        $app->post('/scope', '\VideoController:setScope');
        $app->post('/like', '\VideoController:like');

    });

    $app->group('/payments', function () use ($app) {

        $app->get('/sdk/customer', '\PaymentController:getMethods');

        $app->get('/methods', '\PaymentController:getMethods');
        $app->post('/methods', '\PaymentController:addMethod');

        $app->get('/plans', '\PaymentController:getPlans');
        $app->post('/subscribe', '\PaymentController:subscribeCustomer');

        $app->post('/charge/{user_id}', '\PaymentController:addCharge');
        $app->get('/coupon/{coupon_id}', '\PaymentController:getCoupon');
        $app->get('/customer/{userId}', '\PaymentController:getCustomer');
        $app->post('/event', '\EventController:handleEvent');

    });

    $app->group('/admin', function () use ($app) {
        $app->delete('/videos/{id:[0-9]+}', '\VideoController:deleteById');
    });
});
