<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ . '/../models/trending.php';
require_once __DIR__ . '/../models/video.php';

// API group
$app->group('/almafind/api/v1', function () use ($app) {

    /**
     * Get Trending item list
     */
    $app->get('/admin/trending', function (Request $request, Response $response, $args) use ($app) {
        $result = Trending::get_all();
        if (!$result['result']) {
            return $response->withStatus(401, $result['error']);
        }

        $response_data = $result['rows'];
        $response->getBody()->write(json_encode($response_data));
        return $response->withHeader('Content-type', 'application/json');
    });

    /**
     * Add new trending item
     */
    $app->post('/admin/trending', function (Request $request, Response $response, $args) use ($app) {
        $error_message = '';

        $data = file_get_contents($_POST['data']);

        $uploadFileName = uniqid().'.png';
        $fp = fopen(__DIR__ . '/../../../uploads/trending/' .$uploadFileName, 'w'); //Prepends timestamp to prevent overwriting
        fwrite($fp, $data);
        fclose($fp);

        $fileName = $_POST['fileName'];
        $trendingName = $_POST['trendingName'];
        $result = Trending::add($trendingName, $uploadFileName, $fileName);
        if (!$result['result']) {
            return $response->withStatus(401, $error_message);
        }

        $row_id = $result['row_id'];
        $result = Trending::get($row_id);
        if (!$result['result']) {
            return $response->withStatus(401, $error_message);
        }

        $response_data = $result['row'];

        $response->getBody()->write(json_encode($response_data));
        return $response->withHeader('Content-type', 'application/json');
    });

    /**
     * Delete
     */
    $app->delete('/admin/trending/{id}', function (Request $request, Response $response, $args) use ($app) {
        $id = $args['id'];
        $uploadPath = $this->get('settings')['uploadPath'];

        // before remove record, delete file.
        $result = Trending::get($id);
        if (!$result['result']) {
            return $response->withStatus(401, $result['error']);
        }

        // before remove record, delete file.
        $imageName = $result['row']['imageName'];
        $imagePath = $uploadPath.'trending/'.$imageName;
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        // reset all videos having trending of it.
        $result = Video::getByTrending($result['row']['tag']);
        if ($result['result']) {
            $rows = $result['rows'];
            foreach($rows as $row) {
                $row['trendingId'] = '';
                $row['trendingName'] = '';
                $u_res = Video::update($row);
                if (!$u_res['result']) {
                    $this->logger->debug('Update failed');
                }
            }
        }

        // delete record
        $result = Trending::delete($id);
        if (!$result['result']) {
            return $response->withStatus(401, $result['error']);
        }

        $response_data = array('result' => 'ok');

        $response->getBody()->write(json_encode($response_data));
        return $response->withHeader('Content-type', 'application/json');
    });
});
