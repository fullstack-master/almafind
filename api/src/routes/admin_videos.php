<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ . '/../models/trending.php';
require_once __DIR__ . '/../models/video.php';

// API group
$app->group('/almafind/api/v1', function () use ($app) {
    /**
     * set trending
     * {
     *     "trendingId": id,
     *     "trendingName": Name
     * }
     */
    $app->post('/admin/videos/{video_id}/trending', function (Request $request, Response $response, $args) use ($app) {
        $video_id = $args['video_id'];
        $request_obj = $request->getParsedBody();

        // before remove record, delete file.
        $result = Video::getVideo($video_id);
        if (!$result['result']) {
            return $response->withStatus(401, $result['error']);
        }

        $video = $result['row'];
        $video['trendingId'] = $request_obj['trendingId'];
        $video['trendingName'] = $request_obj['trendingName'];

        $result = Video::update($video);
        if (!$result['result']) {
            return $response->withStatus(401, $result['error']);
        }

        $response_data = $video;

        $response->getBody()->write(json_encode($response_data));
        return $response->withHeader('Content-type', 'application/json');
    });
});
