<?php

$dsn = getenv("DSN");
$db_user = getenv("DB_USER");
$db_pass = getenv("DB_PASS");
$uploadPath = getenv("UPLOAD_PATH");

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],

        // Database settings
        'database' => [
            'dsn' => $dsn,
            'username' => $db_user,
            'password' => $db_pass,
        ],

        // Upload directory path
        'uploadPath' => $uploadPath,
    ],
];
