<?php
session_start();
$userId = isset($_SESSION['user_id']) ? $_SESSION['user_id']: '';
$cardNumber = isset($_POST['card_number']) ? $_POST['card_number']: '';
$curMonth = date('Y-m');
?>
<html>
<head>
    <title>Payment method</title>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="application/javascript" src="js/jquery.base64.js"></script>
<script>
    var userId = '<?php echo $userId;?>';
    var cardNumber = '<?php echo $cardNumber;?>';

    function addPaymentMethod() {
        var d = new Date($('#expiry').val());
        var expMonth = d.getMonth() + 1;
        var expYear = d.getFullYear();
        var cvc = $('#cvc').val();
        var data = JSON.stringify({
            'exp_month': expMonth,
            'exp_year': expYear,
            'card_number': cardNumber,
            'cvc': cvc
        });
        $.ajax({
            type: "POST",
            url: "api/v1/payments/methods/" + userId,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: data,
            complete: function (response) {
                var card = JSON.parse(response.responseText);
                $('#status')[0].innerHTML = card.id;
            }
        });
        $('#status')[0].innerHTML = 'Please wait...';
    }

    $(document).ready(function() {
        $('#card_number').val(cardNumber);
    })
</script>
<body>
<div align="center">
    <div>Payment</div>
    <table id="payment_method">
        <tr>
            <td><label for="card_number">Number:</label></td>
            <td><input id="card_number" type="text" readonly></td>
        </tr>
        <tr>
            <td><label for="exp_month">Expire Month:</label></td>
            <td><input id="expiry" name="expiry" type="month" required value="<?php echo $curMonth;?>"></td>
        </tr>
        <tr>
            <td><label for="cvc">CVC:</label></td>
            <td><input id="cvc" type="text"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"><input type="button" onclick="addPaymentMethod();" value="Add"></td>
        </tr>
    </table>
    <div id="status"></div>
    <div><a href="payment.php">Return to Payment</a></div>
</div>
</body>
</html>