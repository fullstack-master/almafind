<?php
session_start();
$userId = isset($_SESSION['user_id']) ? $_SESSION['user_id']: '';
$cardId = isset($_GET['cardId']) ? $_GET['cardId']: '';
$couponCode = isset($_GET['couponCode']) ? $_GET['couponCode']: '';
?>
<html>
<head>
    <title>Payment method</title>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="application/javascript" src="js/jquery.base64.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script>
    var stripePublishableKey = 'pk_test_3NwTRVchgzE9U4CrKCjKXbaz';
    Stripe.setPublishableKey(stripePublishableKey);

    var userId = '<?php echo $userId;?>';
    var cardId = '<?php echo $cardId;?>';
    var couponCode = '<?php echo $couponCode;?>';

    $(function() {
        var $form = $('#payment-form');
        $form.submit(function(event) {
            // Disable the submit button to prevent repeated clicks:
            $form.find('.submit').prop('disabled', true);

            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from being submitted:
            return false;
        });
    });

    function populateCardAndCouponCode() {

    }

    $(document).ready(function() {

    })
</script>
<body>
<div align="center">
    <div>Payment</div>
    <div>Charge</div>
    <form action="" method="POST" id="payment-form">
        <span class="payment-errors"></span>
        <div class="form-row">
            <label>
                <span>Card Number</span>
                <input type="text" size="20" data-stripe="number">
            </label>
        </div>

        <div class="form-row">
            <label>
                <span>Expiration (MM/YY)</span>
                <input type="text" size="2" data-stripe="exp_month">
            </label>
            <span> / </span>
            <input type="text" size="2" data-stripe="exp_year">
        </div>

        <div class="form-row">
            <label>
                <span>CVC</span>
                <input type="text" size="4" data-stripe="cvc">
            </label>
        </div>

        <input type="submit" class="submit" value="Submit Payment">
    </form>
    <div><a href="payment.php">Return to Payment</a></div>
</div>
</body>
</html>