<?php
session_start();
$userId = isset($_SESSION['user_id']) ? $_SESSION['user_id']: '';
$cardToken = isset($_GET['cardToken']) ? $_GET['cardToken']: '';
$couponCode = isset($_GET['couponCode']) ? $_GET['couponCode']: '';
?>
<html>
<head>
    <title>Payment method</title>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="application/javascript" src="js/jquery.base64.js"></script>
<script>
    var userId = '<?php echo $userId;?>';
    var cardToken = '<?php echo $cardToken;?>';
    var couponCode = '<?php echo $couponCode;?>';

    function showChargeStatus(result) {
        var statusText =
            'Charge Id: ' + result.charge_id + '<br>' +
            'Amount: ' + result.amount + '<br>' +
            'Status: ' + result.status + '<br>';

        $('#status')[0].innerHTML = statusText;
    }

    function submitNewCharge() {
        var amount = $('#payment_amount').val();
        var chargeRequest = JSON.stringify({
            "amount": amount,
            "source": cardToken,
            "couponCode": couponCode
        });

        $('#status')[0].innerHTML = 'Please waiting...';

        $.ajax({
            type: "POST",
            url: "api/v1/payments/charge/" + userId,
            contentType: "application/json; charset=utf-8",
            data: chargeRequest,
            dataType: "json",
            complete: function (response) {
                var result = JSON.parse(response.responseText);
                showChargeStatus(result);
            }
        });
    }

    function getCoupon() {
        $.ajax({
            type: "GET",
            url: "api/v1/payments/coupon/" + userId,
            contentType: "application/json; charset=utf-8",
            data: chargeRequest,
            dataType: "json",
            complete: function (response) {
                var result = JSON.parse(response.responseText);
                showChargeStatus(result);
            }
        });
    }

    $(document).ready(function() {
    })
</script>
<body>
<div align="center">
    <div>Payment</div>
    <div>Charge</div>
    <form action="" id="payment-form">
        <label for="payment_amount">Amount: </label>
        <input id="payment_amount" type="text" value="100">
        <input type="button" value="Submit Payment" onclick="submitNewCharge();">
    </form>
    <div id="status"></div>
    <div><a href="payment.php">Return to Payment</a></div>
</div>
</body>
</html>