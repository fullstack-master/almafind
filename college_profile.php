<?php
$college_id = isset($_GET['collegeId']) ? $_GET['collegeId']: '';
?>
<html>
<head>
    <title>College Profile</title>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script>
    var collegeId = '<?php echo $college_id; ?>';

    function getCollegeProfile(collegeId) {
        $.ajax({
            type: "GET",
            url: "/almafind/api/v1/college/" + collegeId,
            complete: function (response) {
                location.href = "trending.php";
            }
        });
    }

    $(document).ready(function() {
        if (collegeId != '') {

        }
    })
</script>
<body>
<div align="center">
    <div><h1>almafind</h1></div>
    <div><a href="menu.php">Menu</a></div>
    <div>College Profile</div>
    <table>
        <tr>
            <td colspan="2">
                <img id="college_photo" src=""/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label id="collegeName"></label>
            </td>
        </tr>
        <tr>
            <td>
                Favorites: <div id="like"></div>
            </td>
            <td>
                Following: <div id="following"></div>
            </td>
        </tr>
        <tr>
            <td>
                Favorites: <div id="like"></div>
            </td>
            <td>
                Following: <div id="following"></div>
            </td>
        </tr>
    </table>
    <input type="button" onclick="handleSignIn()" value="Sign In">
    <div id="status">
    </div>
</div>
</body>
</html>

