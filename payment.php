<?php
session_start();
$userId = isset($_SESSION['user_id']) ? $_SESSION['user_id']: '';
?>
<html>
<head>
    <title>Payment method</title>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="application/javascript" src="js/jquery.base64.js"></script>
<script>
    var userId = '<?php echo $userId;?>';

    function showPaymentMethods(payment_methods) {
        var row, col, imgUrl, pm;
        var oTable = $('#payment_method_list > tbody')[0];

        $('#payment_method_list tbody > tr').remove();

        // section
        row = oTable.insertRow($('#payment_method_list tbody > tr').length);

        col = row.insertCell(0);
        col.colSpan = 3;
        col.style.backgroundColor = '#DDDDDD';
        col.innerHTML = "Payment Methods";

        $.each(payment_methods, function(k, v) {

            row = oTable.insertRow($('#payment_method_list tbody > tr').length);

            col = row.insertCell(0);
            col.style = 'width: 100px';
            imgUrl = 'images/' + v.brand + '.png';
            col.innerHTML = "<img src='" + imgUrl + "' width='100px' height='50px' />";

            col = row.insertCell(1);
            col.style = 'width: 200px';
            col.innerHTML = '*' + v.last4;

            col = row.insertCell(2);
            col.innerHTML = "<input type='button' onclick=goChargePage('" + v.cardToken + "') value='Charge'/>";

            if (v.default) pm = v;
        });

        // insert coupon code
        row = oTable.insertRow($('#payment_method_list tbody > tr').length);

        col = row.insertCell(0);
        var imgUrl = 'images/almafind.png';
        col.innerHTML = "<img src='" + imgUrl + "' width='100px' height='50px' />";

        col = row.insertCell(1);
        col.style = 'width: 200px';
        col.innerHTML = "<input id='coupon_code' type='text' maxlength='15' placeholder='Add coupon code'>";

        // horizontal line
        row = oTable.insertRow($('#payment_method_list tbody > tr').length);

        col = row.insertCell(0);
        col.colSpan = 3;
        col.innerHTML = "<hr>";

        showNewPaymentMethods();
        showDefaultPaymentMethods(pm);
    }

    function showNewPaymentMethods() {
        var oTable = $('#new_payment_method > tbody')[0];

        // section
        row = oTable.insertRow($('#new_payment_method tbody > tr').length);

        col = row.insertCell(0);
        col.colSpan = 3;
        col.style.backgroundColor = '#DDDDDD';
        col.innerHTML = "Add Payment Method";

        row = oTable.insertRow($('#new_payment_method tbody > tr').length);
        col = row.insertCell(0);
        col.style = 'width: 100px';
        imgUrl = 'images/payment_plus.png';
        col.innerHTML = "<img src='" + imgUrl + "'/>";

        col = row.insertCell(1);
        col.style = 'width: 200px';
        col.innerHTML = "<input id='card_number' name='card_number' type='text' maxlength='16' pattern='[0-9]{13,16}' placeholder='Add Credit Card'>";

        col = row.insertCell(2);
        col.innerHTML = "<input type='submit' value='Add'/>";

        // horizontal line
        row = oTable.insertRow($('#new_payment_method tbody > tr').length);

        col = row.insertCell(0);
        col.colSpan = 3;
        col.innerHTML = "<hr>";
    }

    function showDefaultPaymentMethods(pm) {
        var row, col, imgUrl;
        var oTable = $('#default_payment > tbody')[0];

        // section
        row = oTable.insertRow($('#default_payment tbody > tr').length);

        col = row.insertCell(0);
        col.colSpan = 3;
        col.style.backgroundColor = '#DDDDDD';
        col.innerHTML = "Payment Default";

        if (pm) {
            row = oTable.insertRow($('#default_payment tbody > tr').length);

            col = row.insertCell(0);
            col.style = 'width: 100px';
            imgUrl = 'images/payment_person.png';
            col.innerHTML = "<img src='" + imgUrl + "'/>";

            col = row.insertCell(1);
            col.style = 'width: 200px';
            col.innerHTML = 'Personal' + '<br>*' + pm.last4;

            col = row.insertCell(2);
            col.innerHTML = "<input type='button' onclick=goChargePage('" + pm.cardToken + "') value='Charge'/>";
        }
    }

    function goChargePage(cardToken) {
        var couponCode = $('#coupon_code').val();
        location.href = 'charge_indirect.php?cardToken=' + cardToken + '&couponCode=' + couponCode;
    }

    function getPaymentMethodList() {
        $.ajax({
            type: "GET",
            url: "api/v1/payments/methods/" + userId,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            complete: function (response, status) {
                var result = JSON.parse(response.responseText);
                if (result) {
                    showPaymentMethods(result['data']);
                }
            }
        });
    }

    $(document).ready(function() {
        getPaymentMethodList();
    })
</script>
<body>
<div align="center">
    <div>Payment</div>
    <table id="payment_method_list"><tbody></tbody></table>
    <form method="post" action="card.php">
        <table id="new_payment_method"><tbody></tbody></table>
    </form>
    <table id="default_payment"><tbody></tbody></table>
</div>
</body>
</html>