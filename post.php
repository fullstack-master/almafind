<?php
$userId = isset($_GET['userId']) ? $_GET['userId']: '';
?>
<html>
<head>
    <title>Post</title>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="application/javascript" src="js/jquery.base64.js"></script>
<script>

    function getSchoolTags() {
        $.ajax({
            type: "GET",
            url: "api/v1/colleges/tags",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            complete: function (response) {
                var tags = JSON.parse(response.responseText);
                $.each(tags, function(key, value) {
                    $("#tagSchool")
                        .append($("<option></option>")
                        .attr("value", value.collegeId)
                        .text(value.collegeName));
                });
            }
        });
    }

    function postVideo(videoInfo) {
        $.ajax({
            type: "POST",
            url: "api/v1/videos",
            contentType: "application/json; charset=utf-8",
            data: videoInfo,
            success: function (result) {
                $('#status')[0].innerText = result.message + " videoId: " + result.videoId;
            },
            error: function (response) {
                var msg = $.parseJSON(response.responseText);
                console.log(msg);
            }
        });
    }

    function handlePost() {
        if ($('#videoUrl').val() == '') {
            alert('Please choose video file first.');
            return;
        }

        var video_info = {
            posterId: $('#userId').val(),
            videoUrl: $('#videoUrl').val(),
            fileName: $('#fileName').val(),
            caption: $('#caption').val(),
            hashTags: '',
            schoolTag: ''
        };

        postVideo(JSON.stringify(video_info));
    }

    $(document).ready(function() {

        $('#video').change(function(e) {
            var file = $('#video')[0].files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (event) {
                var result = event.target.result;
                var fileName =$('#video')[0].files[0].name;
                $.ajax({
                    type: 'post',
                    url: 'api/v1/videos/upload',
                    data: { data: result, name: fileName },
                    dataType: 'application/json',
                    complete: function (response) {

                        var video = $.parseJSON(response.responseText);

                        $('#videoUrl').val(video.videoUrl);
                        $('#fileName').val(fileName);

                        var d = new Date();
                        var videoPlayer = $('#videoPlayer')[0];
                        var source = document.createElement('source');

                        source.setAttribute('src', 'uploads/videos/' + video.videoUrl + '?' + d.getTime());
                        videoPlayer.appendChild(source);
                        videoPlayer.play();
                    }
                });
            };
        });

        $('#addHashTag').click( function(e) {
            alert('clicked');
        });

        getSchoolTags();
    })
</script>
<body>
<div align="center">
    <div><h1>almafind<h1></div>
    <div><a href="menu.php">Menu</a></div>
    <table>
        <tr>
            <td>
                <label for="video">Video:</label>
            </td>
            <td>
                <input id="userId" type="hidden" value="<?php echo $userId; ?>">
                <input id="video" type="file" name="video">
                <input id="videoUrl" type="hidden" name="videoUrl" value="">
                <input id="fileName" type="hidden" name="fileName" value="">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <video id="videoPlayer" width="320" height="240" controls></video>
            </td>
        </tr>
        <tr>
            <td>
                <label for="caption">Caption:</label>
            </td>
            <td>
                <input id="caption" type="text" name="caption" value="My Caption" placeholder="Write a caption">
            </td>
        </tr>
        <tr>
            <td>
                <label for="caption">Add Hashtags:</label>
            </td>
            <td>
                <input id="hashtag" type="text" name="hashtag" value="" placeholder="hashtag">
                <input id="addHashTag" type="button" name="addHashtag" value="add">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ul id="hashtags">
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <label for="tagSchool">Tag School</label>
            </td>
            <td>
                <select id="tagSchool" name="tagSchool"></select>
            </td>
        </tr>
    </table>
    <input type="button" onclick="handlePost()" value="Post">
    <div id="status"></div>
</div>
</body>
</html>