<html>
<head>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="application/javascript" src="js/jquery.base64.js"></script>
<script>

    function showUserInfo(user) {
        $('#userId')[0].innerText = "userId: " + user.userId;

        $('#home_link')[0].href = 'recent.php?userId=' + user.userId;
        $('#post_link')[0].href = 'post.php?userId=' + user.userId;
        $('#link').removeAttr('hidden');
        if (user.verified == 1) {
            $('#post').removeAttr('hidden');
        }
    }

    function getUserInfo(token) {
        $.ajax({
            type: "GET",
            url: "api/v1/users/me",
            dataType: "json",
            beforeSend: function(xhr) {xhr.setRequestHeader('Authorization', token)},
            error: function (response) {
                $('#status')[0].innerHTML = "Exception: " + response.responseText;
            },
            success: function (result) {
                if (result.error != '')
                    $('#status')[0].innerText = "Error: " + result.error;
                else
                showUserInfo(result.data);
            }
        });
    }

    function basicLogin(email, password) {
        var auth = "Basic " + btoa(email + ":" + password);
        $.ajax({
            type: "GET",
            url: "api/v1/login",
            dataType: "json",
            beforeSend: function(xhr) {xhr.setRequestHeader('Authorization', auth)},
            error: function (response) {
                $('#status')[0].innerHTML = "Exception: " + response.responseText;
            },
            success: function (result) {
                if (result.error != '')
                    $('#status')[0].innerText = "Error: " + result.error;
                else {
                    $('#token')[0].innerText = "userId: " + result.data.token;
                    getUserInfo(result.data.token);
                }
            }
        });
    }

    function handleSignIn() {
        var data = {
            email: $('#email').val(),
            password: $('#password').val(),
        };
        basicLogin(data.email, data.password);
    }
</script>
<body>
<div align="center">
    <div>
        Sign In
    </div>
    <table>
        <tr>
            <td>
                <label for="email">E-mail:</label>
            </td>
            <td>
                <input id="email" type="text" placeholder="E-Mail" name="email" value="aaa@aaa.com">
            </td>
        </tr>
        <tr>
            <td>
                <label for="password">password:</label>
            </td>
            <td>
                <input id="password" type="password" name="password" value="aaa">
            </td>
        </tr>
    </table>
    <input type="button" onclick="handleSignIn()" value="Sign In">
    <div id="status"></div>
    <div id="userId"></div>
    <div id="token"></div>
    <div id="link" hidden>
        <a id='home_link' href="recent.php">HOME</a>
    </div>
    <div id="post" hidden>
        <a id='post_link' href="post.php">Post</a>
    </div>
</div>
</body>
</html>

