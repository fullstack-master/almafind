<html>
<head>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script>
    function postSignUp(signUpInfo) {
        $.ajax({
            type: "POST",
            url: "api/v1/sign_up",
            contentType: "application/json; charset=utf-8",
            data: signUpInfo,
            success: function (result) {
                $('#status')[0].innerText = result.message + " userId: " + result.userId;
            },
            error: function (response) {
                var msg = $.parseJSON(response.responseText);
                console.log(msg);
            }
        })
    }

    function handleSignUp() {
        var sign_up_info = {
            userName: $('#userName').val(),
            email: $('#email').val(),
            photoId: $('#photoId').val(),
            password: $('#password').val(),
            overview: $('#overview').val()
        };

        postSignUp(JSON.stringify(sign_up_info));
    }

    $(document).ready(function() {
        $('#photo')[0].onchange = function(e) {
            var file = $('#photo')[0].files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (event) {
                var result = event.target.result;
                var fileName = $('#photo')[0].files[0].name;
                $.ajax({
                        type: 'post',
                        url: 'api/v1/upload_photo_json',
                        data: { data: result, name: fileName },
                        dataType: 'application/json',
                        complete: function (response) {
                            var photo = $.parseJSON(response.responseText);
                            var d = new Date();
                            $('#photoImage').attr('src', 'uploads/photos/' + photo.photoId + '?' + d.getTime());
                            $('#photoId').val(photo.photoId);
                            $('#status')[0].innerHTML = 'the image was uploaded.';
                        }
                });
                $('#status')[0].innerHTML = 'uploading image...';
            };
        };
    });
</script>
<body>
<div align="center">
    <div>
        User Register
    </div>
    <table>
        <tr>
            <td>
                <label for="userName">User Name:</label>
            </td>
            <td>
                <input id="userName" type="text" placeholder="User Name" name="userName">
            </td>
        </tr>
        <tr>
            <td>
                <label for="email">E-mail:</label>
            </td>
            <td>
                <input id="email" type="text" placeholder="E-Mail" name="email">
            </td>
        </tr>
        <tr>
            <td>
                <label for="password">password:</label>
            </td>
            <td>
                <input id="password" type="password" name="password">
            </td>
        </tr>
        <tr>
            <td>
                <label for="photo">Photo:</label>
            </td>
            <td>
                <input id="photo" type="file" name="userPhoto">
                <input id="photoId" type="hidden" name="userPhotoId">
            </td>
        </tr>
        <tr>
            <td>
                <label for="photoImage">Photo Image:</label>
            </td>
            <td>
                <img id="photoImage" src="" style="width: 100px; height: 100px;">
            </td>
        </tr>
        <tr>
            <td>
                <label for="overview">Overview:</label>
            </td>
            <td>
                <textarea id="overview" rows="4" cols="50" name="overview" form="signUpForm"></textarea>
            </td>
        </tr>
    </table>
    <input type="button" onclick="handleSignUp()" value="Sign Up">
    <div id="status"></div>
    <div><a href="signin.php">Sign In</a></div>
</div>
</body>
</html>

