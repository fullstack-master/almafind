<?php
session_start();

$user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id']: '';

?>
<html>
<head>
    <title>Student Profile</title>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script>
    function getStudentProfile(userId) {
        $.ajax({
            type: "GET",
            url: "api/v1/users/" + userId,
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                $('#userName')[0].innerHTML = result.userName;
                $('#email')[0].innerHTML = result.email;
                $('#likes')[0].innerHTML = result.likes;
                $('#follows')[0].innerHTML = result.follows;
                $('#overview')[0].innerHTML = result.overview;
                var d = new Date();
                $('#photoImage').attr('src', 'uploads/photos/' + result.photoId + '?' + d.getTime());
            },
            error: function (response) {
                var msg = $.parseJSON(response.responseText);
                console.log(msg);
            }
        });
    }

    $(document).ready(function() {
        var userId = $('#userId').val();
        if (userId != '') {
            getStudentProfile(userId);
        }
    })
</script>
<body>
<div align="center">
    <div>
        Student Profile
    </div>
    <input id='userId' type="hidden" value="<?php echo $user_id; ?>">
    <table>
        <tr>
            <td>
                User Name:
            </td>
            <td>
                <div id="userName"></div>
            </td>
        </tr>
        <tr>
            <td>
                e-mail:
            </td>
            <td>
                <div id="email"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <img id="photoImage" src="" style="width: 100px; height: 100px;">
            </td>
        </tr>
        <tr>
            <td>
                Favorites: <div id="likes"></div>
            </td>
            <td>
                Following: <div id="follows"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Overview:
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <textarea id="overview"></textarea>
            </td>
        </tr>
    </table>
</div>
</body>
</html>

