<?php
// calculate time interval
$datetime2 = new DateTime();
$datetime1 = new DateTime('2016-09-20 10:22:53');
$interval = $datetime2->diff($datetime1);
var_dump($interval);

if ($interval->y > 0) {
    echo $interval->format('%y years ago');
} else if ($interval->m > 0) {
    echo $interval->format('%m months ago');
} else if ($interval->d > 0) {
    echo $interval->format('%d days ago');
}

