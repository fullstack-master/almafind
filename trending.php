<html>
<head>
    <title>Recent</title>
</head>
<script type="application/javascript" src="js/jquery-1.7.2.min.js"></script>
<script>
    function showTrendingItem(trending_list) {
        var oTable = $('#trending_item_table > tbody')[0];
        var d = new Date();

        $.each(video_list, function(k, v) {
            var row, col;

            row = oTable.insertRow($('#trending_item_table tbody > tr').length);

            col = row.insertCell(0);
            var imgUrl = 'images/recent_list_icon.png';
            col.innerHTML = "<img src='" + imgUrl + "'/>";
            col = row.insertCell(1);
            col.innerHTML = v.caption;

            row = oTable.insertRow($('#trending_item_table tbody > tr').length);

            col = row.insertCell(0);
            col.colspan = 2;
            col.innerHTML = '<video id="' + v.id +'" width="320" height="240" controls></video>';

            var videoPlayer = $('#' + v.id)[0];
            var source = document.createElement('source');

            source.setAttribute('src', 'uploads/videos/' + v.videoUrl + '?' + d.getTime());
            videoPlayer.appendChild(source);

        });
    }

    function getTrendingVideos() {
        $.ajax({
            type: "GET",
            url: "api/v1/videos/trending",
            dataType: "application/json; charset=utf-8",
            complete: function (response) {
                var video_list = $.parseJSON(response.responseText);
                showVideos(video_list);
            }
        });
    }

    $(document).ready(function() {
        getTrendingVideos();
    });
</script>
<body>
<div align="center">
    <div><h1>almafind<h1></div>
    <div><a href="menu.php">Menu</a></div>
    <table style="text-align: center">
        <tr>
            <td>
                <a href="recent.php">Recent</a>
            </td>
            <td>
                <a href="trending.php">Trending</a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table id="trending_item_table">
                    <tbody></tbody>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>

